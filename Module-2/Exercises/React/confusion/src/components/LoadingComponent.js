//when the dishes are being loaded, it returns a loading spinner icon.
import React from 'react';

export const Loading = () =>{
    return(
        //pulse is used to rotate the spinner around
        <div className="col-12">
            <span className="fa fa-spinner fa-pulse fa-3x fa-fw text-primary"></span>
            <p>Loading . . .</p>
        </div>
    );
};