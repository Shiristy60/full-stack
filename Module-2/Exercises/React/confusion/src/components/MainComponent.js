import React, { Component } from 'react';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import Menu from './MenuComponent';
import DishDetail from './DishdetailComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { postComment, fetchDishes, fetchComments, fetchPromos, fetchLeaders, postFeedback } from '../redux/ActionCreators';
import { actions } from 'react-redux-form'; //to reset the feddback from
import { TransitionGroup, CSSTransition } from 'react-transition-group';

const mapStoreToProps = state =>{
  return{
    dishes: state.dishes,
    comments: state.comments,
    promotions: state.promotions,
    leaders: state.leaders
  }
}

//this receives dispatch as parameter
const mapDispatchToProps = dispatch => ({
  postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)), //this returns the action object for adding a comment, that action object is given as a parameter to the dispatch, which is then supplied as a function.
  fetchDishes: () => {dispatch(fetchDishes())},
  /* fetchDishes thunk is mapped to the props as the property fetchDishes using dispatch in order to dispatch, 
  which when invoked will result in a call to dispatch fetchDishes function that is imported here. */
  fetchComments: () => {dispatch(fetchComments())},
  fetchPromos: () => {dispatch(fetchPromos())},
  fetchLeaders: () => {dispatch(fetchLeaders())},
  postFeedback: (firstname,lastname,telnum,email,agree,contactType,message) =>
    {dispatch(
      postFeedback(
        firstname,
        lastname,
        telnum,
        email,
        agree,
        contactType,
        message
      )
    )},
  resetFeedbackForm: () => {dispatch(actions.reset('feedback'))}
  //does the necessary functions to reset the form.
});

class Main extends Component {

  //after the component gets mounted into the view of the application, whatever is mentioned will be performed.
  componentDidMount(){
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromos();
    this.props.fetchLeaders();
  }
    /* fetchDishes will be called and this will result in a call to fetch the dishes and then load it into the 
    redux store at the point and then when that becomes available, it becomes available for the app. */

  render() {
    const HomePage =  () =>{
      return (
        //in the reducer dishes.js, the state has dishes itself, which contains the any information about the dishes, so to access that, we have to use dishes.dishes
        <Home 
          dish = {this.props.dishes.dishes.filter((dish)=> dish.featured)[0]}
          dishesLoading={this.props.dishes.isLoading}
          dishesErrMess={this.props.dishes.errMess}
          promotion = {this.props.promotions.promotions.filter((promotion) => promotion.featured)[0]}
          promosLoading={this.props.promotions.isLoading}
          promosErrMess={this.props.promotions.errMess}
          leader = {this.props.leaders.leaders.filter((leader)=> leader.featured)[0]}
          leadersLoading={this.props.leaders.isLoading}
          leadersErrMess={this.props.leaders.errMess}
        />)
    }

    const DishWithId = ({match}) => {
      return(
        <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId, 10))[0]}
        isLoading={this.props.dishes.isLoading}
        errMess={this.props.dishes.errMess}
        comments={this.props.comments.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId))}
        commentsErrMess={this.props.comments.errMess}
        postComment={this.props.postComment}
        />
      )
    }

      const RenderLeader = () => {
         return(
           <About leaders={this.props.leaders.leaders} />
         );
    }
    return (
      /*ROUTE MATCHING - 
      <Route>  - path prop enables specification of the current location's pathname.
              - component prop specifies the correponding view for the location.
              - exact attribute ensures that the path must be exactly b matched.
      
      <Switch> - enables grouping together several routes.
              - iterates over all of its children and find the first one that matches the path.*/
      <div>
        <Header />
        <TransitionGroup>
          <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
            <Switch loctaion ={this.props.location}>
              <Route path="/home" component={HomePage}/>
              <Route exact path="/menu" component ={() => <Menu dishes={this.props.dishes}/> }/>
              <Route path="/menu/:dishId" component={DishWithId}/>
              <Route exact path="/aboutus" component={RenderLeader} />
              <Route exact path="/contactus" component={ () => <Contact resetFeedbackForm={this.props.resetFeedbackForm} postFeedback={this.props.postFeedback}/> } />
              <Redirect to="/home"/>
            </Switch>
          </CSSTransition>
        </TransitionGroup>
        <Footer/>
      </div>
    );
  }
}

//connect makes addComment available to the main component
export default withRouter(connect(mapStoreToProps, mapDispatchToProps)(Main));