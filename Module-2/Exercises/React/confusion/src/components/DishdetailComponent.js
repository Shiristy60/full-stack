import React, {Component} from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Modal, ModalHeader, ModalBody, Row, Col, Label} from 'reactstrap';
import { Breadcrumb, BreadcrumbItem, Button } from 'reactstrap';
import {LocalForm, Control, Errors} from 'react-redux-form';
import { Link } from 'react-router-dom';
import{ Loading } from './LoadingComponent';
import {baseUrl} from '../shared/baseURL';
import {FadeTransform, Fade, Stagger} from 'react-animation-components';

//functional component
function RenderDish({dish}){ 
    return(
        <FadeTransform in
            transformProps={{
                exitTransform: 'scale(0.5) translateY(-50%)'
            }}>
            <Card>
                <CardImg top src={baseUrl + dish.image} alt={dish.name} />
                    <CardBody>
                        <CardTitle>
                            {dish.name}
                        </CardTitle>
                        <CardText>
                            {dish.description}
                        </CardText>
                </CardBody>                
            </Card>
        </FadeTransform>
    );
}

//functional component
function RenderComments({comments, postComment, dishId}){
    if(comments!= null)
        return(
            <div className="col-12 col-md-5 m-1">
                <h4>Comments</h4>
                <ul className="list-unstyled">
                    <Stagger in>
                        {comments.map((comment) => {
                            return(
                                <Fade in>
                                    <li key={comment.id}>
                                        <p>{comment.comment}</p>
                                        <p>-- {comment.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit' }).format(new Date(Date.parse(comment.date)))}</p>
                                    </li>
                                </Fade>
                            )    
                        })}   
                    </Stagger>
                </ul>  
                <CommentForm dishId={dishId} postComment={postComment}/>        
            </div>
        );
    else
        return(<div></div>)
}

const DishDetail = (props) => {
    if(props.isLoading){ //when dishes are loading
        return(
            <div className="container">
                <div className="row">
                    <Loading />
                </div>
            </div>
        )
    }
    else if(props.errMess){ //when dishes failed to load
        return(
            <div className="container">
                <div className="row">
                    <h4>{props.errMess}</h4>
                </div>
            </div>
        )
    }
    else if(props.dish != null){ //dishes loaded successfully
        return(
            <div class="container">
                <div className="row">
                    <Breadcrumb>

                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>                
                </div>
                <div className="row">
                    <div className="col-12 col-md-5 m-1">
                        <RenderDish dish={props.dish} />
                    </div>
                        <RenderComments comments= {props.comments} 
                            postComment={props.postComment}
 /* to know the corresponding dishid*/dishId={props.dish.id}
                        />
                </div>
            </div>
        )
    }
    else{
        return(
            <div></div>
        );
    }
}
export default DishDetail;

const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

export class CommentForm extends Component{
    constructor(props){
        super(props);
        this.toggleModal=this.toggleModal.bind(this);
        this.state = {
            isModalOpen: false
        }
    }

    toggleModal(){
        this.setState({isModalOpen:!this.state.isModalOpen});
    }

    handleSubmit(values){
        this.props.postComment(this.props.dishId, values.rating, values.author, values.comment)
    }

    render(){
        return(
            <div>
                <Button outline onClick={this.toggleModal}>
                    <span className="fa fa-pencil fa-lg"></span>
                    Submit Comment
                </Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className="form-group">
                                <Label htmlFor="rating" md={12}>Rating</Label>
                                <Col md={12}>
                                    <Control.select model=".rating" name="rating" id="rating" className="form-control" >
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Control.select>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="author" md={12}>Your Name</Label>
                                <Col md={12}>
                                    <Control.text model=".author" name="author" id="author" className="form-control" validators={{minLength: minLength(3), maxLength:maxLength(15)}}/>
                                    <Errors className="text-danger" model=".author" show="touched" messages={{minLength: 'Must be greater than 2 characters', maxLength:'Must be 15 characters or less'}}/>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="comment" md={12}>Comment </Label>
                                <Col md={12}>
                                    <Control.textarea model=".comment" name="comment" id="author" className="form-control" rows={6}/>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col>
                                    <Button type="submit" color="primary">Submit</Button>
                                </Col>
                            </Row>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </div>
        )
    }

}