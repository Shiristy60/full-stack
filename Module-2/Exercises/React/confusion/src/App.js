import React, { Component } from 'react';
import Main from './components/MainComponent';
import {BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';
import { ConfigureStore } from './redux/configureStore';
import './App.css';

const store = ConfigureStore();

class App extends Component {
  render() {
    return (
      /*  -> REACT ROUTER     - collection of navigational components.
          (react-router-dom)  - enables navigation among views.
                              - uses a browser based bookmarkable URL as an instruction to navigate to a clet-generated view in app.
              <BrowserRouter> - router component.
                              - provides a specialized history object that enables us to navigate among the pages 
              
              <HashRouter> - for static file server */
      <Provider store = {store}>
        <BrowserRouter>
          <div className="App">
            <Main />
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;

//a software LIBRARY is a collection of implementations of behaviour with a well-defined interface. For reuse of behaviour
// a software FRAMEWORK is in which software provides generic functionality that can be selectively changed by additional user-written code


/* IMPERATIVE APPROACH 
  - the designer specifies how the work needs to be accomplished/executed.
   DECLARATIVE APPROACH 
  - the designer only specifies what needs to be done and leaves it to the framework to decide 
 'how' part. 
  
 * REACT follows declarative approach.

 COMPONENT BASED APPROACH : we encapsulate behaviours into small units called components.

  -> create-react-app <name of the web app> : creates the react app.
  -> REACT ELEMENT - a plain JS object which is the smallest building block of react apps.
  
  JSX - shorthand notation to represent JS function calls that evaluate to JS objects. Avoids artificial separation of
        rendering logic from UI logic
  
  -> Components enable us to break down the UI in to smaller reusable pieces.
  -> Each copmponent can store its local information in its state. this can be passed to its children through props. 
     Declared in the constructor. setState is used to change state.
     
  -> Lifting of state is done when several components share same data.

    TYPES OF COMPONENTS -
    * Presentational/Skinny/Dumb/Stateless : How things look. 
                                           : Render the view based on the data that is passed to them in props. 
                                           : Do not maintain their own local state.

    * Container/Fat/Smart/Stateful : Responsible for making things work (Data fetching, state updates). 
                                   : Make use of presentational components for rendering.
                                   : Provide the data to the presentational components. 
                                   : Maintain state and communication with data sources.

  -> LIFECYCLE METHODS - Stages:
                        1. Mounting - the process of outputting the virtual representation of a component into the final UI representation
                        2. Updating
                        3. Unmounting - the next phase in the lifecycle is when a component is removed from the DOM.

      UPDATING LIFECYCLE METHODS            - getDerivedStateFromProps()
      (caused by changes to props/state)    - shouldComponentUpdate() : if a component updates when it is mounted.
                                            - render() :rendering is the process of transforming your react 
                                                        components into DOM (Document Object Model) nodes that your 
                                                        browser can understand and display on the screen.
                                            - getSnapshotBeforeUpdate() : to remember the scrolling position.
                                            - componentDidUpdate() : when a component gets updated.
      
  -> FUNCTIONAL COMPONENT - returns a react elementor a collection of react elements that define the view.
                          - usually used when a component doesn't need a state and any access to lifecycle methods.
                          - recieves props as parameters.
                          - doesn't have local state. 
  -> BROWSER DOM - a browser object
  
  -> VIRTUAL DOM -  lightweight representation of a React object.
                 -  tree data structure of plain JS objects.
                 -  manipulations are extremely fast compared to the browser DOM
                 -  created from scratch on every setState.
                 - The virtual DOM (VDOM) is a programming concept where an ideal, or “virtual”, representation of a 
                   UI is kept in memory and synced with the “real” DOM by a library such as ReactDOM. This process is 
                   called reconciliation.
  
  -> DIFFING ALGORITHM - detects those nodes that are changed.
                       - updates the entire sub-tree if diffing detects that two elements are of different types.
  
  SINGLE PAGE APPLICATION - SPAs are all about serving an outstanding UX by trying to imitate a “natural” environment in the browser .
                          — no page reloads, no extra wait time. 
                          - It is just one web page that you visit which then loads all other content using JavaScript which they heavily 
                            depend on.

  FORMS - 1. CONTROLLED
          2. UNCONTROLLED
  
  * In HTML every form element maintains its own state.

  CONTROLLED FORMS :
    - make the react component control the form that it renders.
    - typing the form state to component state.
    - every state change will have an associated handler function.

  FORM FEEDBACK - lets display any error messages to the user.

  UNCONTROLLED FORMS :
    - allows to handle the form data by the DOM itself.
    - use a 'innerRef' to get form values from the DOM.

  
  MVC (Model-View-Controller) :
    - isolation of domain logic from user interface.
    - permits independent development, testing and maintenance.

    MODEL - manages the behavior and data of the application domain.
          - responds to requests for information about its state (usually from the view).
          - responds to instructions to change state.
          - in event driven systems, notifies observers (usually views) when the information changes so that they can react.

    VIEW  - renders the model into a form suitable for interaction, typically a user interface element.
          - multiple views can exist for a single model for different purposes.
          - a viewport typically has a one to one correspondence with a display surface and knows how to render to it.

    CONTROLLER - receives user input and initiates a response by making calls on model objects.
               - accepts input from the user and instructs the model and viewport to perform actions based on that input.

  FLUX ARCHITECTURE 
    - unidirectional data flow.
    - store for an application is the state, business logic that modifies the state.
    - the only way the application state within the store is by requesting the store.
    - request to change the state are through actions which are synced to the dispatcher.
    - the dispatcher becomes a controlling unit for serializing any actions that you request for changing the store.
    - the main component gets its state from the store of the flux architecture.
    - the controller views subscribe to the store and they will have access to getters on the store.
    - when the store updates its state, the store will emit a change which the controller views will watch for.
    - whenever a change is emitted then the controller views will go back and be able to get the updated state, which results in re rendering of parts of the views.
    - views cannot directly change the data store.

  REDUX
    - general approach that supports a way of managing store.
    - state container for JS apps.
    - make state mutations predictable.
    - single state object tree within a single store.
    - state is only read-only: changes should only be done through actions.
    - changes are made with pure functions: take previous state and action and return next state, no mutation of the previous state. 

    REACT-REDUX-FORM :
      - library for craeting forms using redux.
      - collection of reducer creators and action creators.
      - form data stored in Redux store in a model
      - validation support for forms.
      - the form state persists even after navigating from one page to another.

      LocalForm :
        - maps form model to local state of the component.
        - suitable when form data persistence across component mounting/unmounting, is not needed.
        - can perform form validation.

    REDUX ACTIONS :
      - payloads of information that send data from app to the store - done through store.dispatch()
      - plain JS object that must have
        * a type property that indicates the type of action to be performed.
        * rest of the object contains the data necessary for the action (payload).
        * when this action is delivered to a reducer function, the reducer function interprets what type of action it is and then initiate the appropriate action to change the state of the store.
      
      ACTION CREATORS :
        - generate action.
        - encapsulate the process of creating the action objects.
        - return the action action object.
        - resulting action object can be passed to the store through dispatch().

      REDUCERS :
        - within store, reducers take in actions as one of the parameters and then takes in the previous state of the current state of the application as the other parameter, and that generates the used state. (basically, generates the next state without mutating the previous state).
    
    REDUX MIDDLEWARE :
      - provides the capability to run code after an action is dispatched, but before it reaches the reducer.
        * third-party extension point
        * ex - logging, async API calls.
      - forms pipeline that wraps around the dispatch()
      - pass actions onward.
      - restart the dispatch pipeline.
      - access the store state.
      - inspecting the actions and the state.
      - modify actions
      - dispatch other actions
      - stop actions from reaching the reducers, etc.

      How to use : 
        - applyMiddleware()
        - returns a "store enhacer" that is passed to createStore().

    THUNK :
      - subroutine that is used to inject an additional calculation in another subroutine.
      - delay a calculation until its result is needed
      - insert operations at the beginning or end of the other subroutine.

    REDUX THUNK :
      - middleware that allows us to write action creators that return a function instead of an action.
        * can be used to delay the dispatch of an action, or
        * dispatch only if a certain condition is met.
      - inner function receives the dispatch() and getState() store methods.
      - useful for complex synchronous logic
        * multiple dispatches
        * conditional dispatches
        * simple async logic
  
  NETWORKING ESSENTIALS :
    - JSON (JavaScript Object Notation) : a way of encoding data that is shipped from the server side to the client side or vice versa.
    - XML : way of encoding data when in transhipment between the client and server side.
    - SOAP : protocol that allows communication between entities distributed over network. XML based.
    - HTTP : client-server protocol. 
           : allows retrieving inter-linked text documents. 
           : used for encoding the messages that are exchanged between client app and server side.
           : a request is sent from the client app to the server and this is encoded in the form of an HTTP request message.
           : the request message typically carries a URL in the request message indicationg what is it that is required from the server.
  
  PROMISES :
    - a mechanism that supports asynchronous computation.
    - represents a value that may be available now, or in the future, or never.
    - provides a mechanism for a value that is not available at the moment that you asked for.
    - when the result becomes available it'll come back and deliver the results.
  */  