// string constants specifying corresponding action.
//import these action types into the reducer functions and then use that to do the matching in the switch statement in configureStore.
export const ADD_COMMENT = 'ADD_COMMENT';
export const DISHES_LOADING = 'DISHES_LOADING';
export const DISHES_FAILED = 'DISHES_FAILED';
export const ADD_DISHES = 'ADD_DISHES';

//from the server
//when we first render the app, we'll first render the home component. By the time home component is rendered , the comments will also be fetched in. So, by the time we navigate to the dish detail component, the comments would already been fetched in.
export const ADD_COMMENTS = 'ADD_COMMENTS';
export const COMMENTS_FAILED = 'COMMENTS_FAILED';

export const PROMOS_LOADING = 'PROMOS_LOADING';
export const ADD_PROMOS = 'ADD_PROMOS';
export const PROMOS_FAILED = 'PROMOS_FAILED';

export const LEADERS_LOADING = 'LEADERS_LOADING';
export const ADD_LEADERS = 'ADD_LEADERS';
export const LEADERS_FAILED = 'LEADERS_FAILED';

export const ADD_FEEDBACK ='ADD_FEEDBACK';
