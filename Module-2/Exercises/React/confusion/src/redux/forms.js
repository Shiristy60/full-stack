export  const InitialFeedback = {
    firstname : '',
    lastname : '',
    telnum : '',
    email : '',
    agree : false,
    contactType : 'Tel.',
    message : ''
}

//react redux form keeps track of the state of the form and persists it.