import { createStore, combineReducers, applyMiddleware } from 'redux';
import {createForms} from 'react-redux-form'; //configure redux to store the form. Lets us add form state into the store 
import { Dishes } from './dishes';
import { Promotions} from './promotions';
import { Leaders } from './leaders';
import { Comments} from './comments';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { InitialFeedback } from './forms';

export const ConfigureStore = () =>{
    //createStore takes enhancer as the second parameter, which in this case is will be returned by applyMiddleware.
    const store = createStore(
        combineReducers({ //combines the different reducers to a global state.
            dishes: Dishes,
            comments: Comments,
            leaders: Leaders,
            promotions: Promotions,
            ...createForms({
                feedback: InitialFeedback // adds the necessary reducer functions and also state information into the create store. RRF has its form functions
            })
        }),
        applyMiddleware(thunk, logger)
    );
    return store;
}