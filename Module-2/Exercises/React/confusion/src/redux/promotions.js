import * as ActionTypes from './ActionTypes';

export const Promotions = (state={
    isLoading: true, 
    errMess: null, 
    promotions:[] 
    }, action) => {
    switch(action.type) {
        case ActionTypes.ADD_PROMOS: 
            //...state means it takes current value of the state and any further changes will be applied as modifications to this state. A new object is created from the existing state and return that object.
            //when the ADD_DISHES is passed into this reducer, then then whatever is passed in as the parameter there in the payload of the action object, that will be set equal to that dishes there.
            return {...state, isLoading: false, errMess: null, promotions: action.payload}; //action.payload carries the dishes information
        case ActionTypes.PROMOS_LOADING:
            return {...state, isLoading: true, errMess: null, promotions: []}
        case ActionTypes.PROMOS_FAILED:
            return{...state, isLoading:false, errMess: action.payload, promotions:[]};
        default :
            return state;
    }
}